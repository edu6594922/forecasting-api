from contextlib import asynccontextmanager
from typing import List

import numpy as np
from fastapi import FastAPI
from tensorflow.keras.models import load_model


@asynccontextmanager
async def lifespan(api: FastAPI):
    global loaded_model
    loaded_model = load_model("model/LSTM forecasting model.keras")

    # Sample data for cold-starting the model
    sample_input_data = [8.85, 9.05]
    forecast = await predict_output(loaded_model, sample_input_data)
    print("Model cold-started with sample data:", forecast)
    yield


api = FastAPI(lifespan=lifespan)


x = "xyz"


async def predict_output(model, input_data):
    num_forecasts = 8 - len(input_data)
    for _ in range(num_forecasts):
        input_np = np.array(input_data)
        input_re = np.reshape(input_np, (1, len(input_data), 1))
        yhat = model.predict(input_re, verbose=0)
        yhat = round(yhat[0][0], 2)
        input_data.append(yhat)

    return input_data


@api.post("/forecast")
async def forecast_endpoint(input_data: List[float]):
    # convert input data to list
    input_data = [float(i) for i in input_data]
    forecast = await predict_output(loaded_model, input_data)
    forecast_str = [str(i) for i in forecast]
    return {"forecast": forecast_str}
