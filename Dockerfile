# Use a Python base image
FROM python:3.11-slim

# Set the working directory in the container
WORKDIR /forecasting-API

# Install Poetry
RUN pip install poetry

RUN poetry config virtualenvs.in-project true

# Copy only the pyproject.toml and poetry.lock to leverage Docker cache
COPY pyproject.toml poetry.lock ./

# Install Python dependencies using Poetry
RUN poetry install --no-root

# Copy the rest of the application code
COPY . .

# Expose the port your FastAPI server will run on
EXPOSE 5000
